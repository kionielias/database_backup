from celery import Celery
import copy
from django.conf import settings
from django.core import management


app = Celery()

@app.task
def backup_db():
    previouse_db_configuration = copy.deepcopy(settings.DATABASES)
    # hack hack !!!!!
    # its not recommended to alter django settings at runtime
    # doing this for django-db backup to work even if Database settings has not been defined
    settings.DATABASES = settings.DBBACKUP_DATABASES
    management.call_command('dbbackup')
    settings.DATABASES = previouse_db_configuration