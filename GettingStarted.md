## CLONE THIS REPO

###  Create a virtualenv 

### sudo apt-get install -y postgresql-common postgresql-client

### Install the requirements

### export the database URL for the postgres you want to back

### export the access token of the dropbox account to store the back up to

### run the web server to get an interface to back up and restore the database

### if you intend to run a periodic task to back up export the rabbit_mq details

### export BACK_AFTER_SECONDS=how often to do back in seconds

~~~
RABBIT_MQ_HOST = host url of rabbit server

RABBIT_MQ_PORT = port the server is running on

RABBIT_MQ_USERNAME = username to use 

RABBIT_MQ_PASSWORD = password to use

RABBIT_MQ_VHOST = virtual host(not mandatory)
~~~``